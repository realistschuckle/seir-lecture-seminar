def parse_integer_safe(s: str) -> int:
    try:
        value = int(s)
    except:
        value = 0
    return value


def add_two_numbers(x: str, y: str) -> int:
    x = x.strip()  # Same as .trim() in JS
    y = y.strip()  # Same as .trim() in JS
    value = parse_integer_safe(x)
    value += parse_integer_safe(y)
    return value

x = "   3 "
y = " 4   "
print(x, "+", y, "=", end=" ")
print(add_two_numbers(x, y))

y = "  a  "
print(x, "+", y, "=", end=" ")
print(add_two_numbers(x, y))

